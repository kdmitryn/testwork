from rest_framework import serializers
from . models import Landline


class LandlineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Landline
        fields = [
            'pk',
            'll_number',
            ]
