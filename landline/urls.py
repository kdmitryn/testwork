from django.urls import path
from .views import LandlineListView, LandlineCreateView, LandlineUpdateView
from .views import LandlineDeleteView, LandlineDetailView
from .views import LandlineAPIview

from django.urls import include
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'landline', LandlineAPIview)


urlpatterns = [
    path('landline/list/', LandlineListView.as_view()),
    path('landline/create/', LandlineCreateView.as_view()),
    path('landline/<int:pk>/', LandlineDetailView.as_view()),
    path('landline/update/<int:pk>/', LandlineUpdateView.as_view()),
    path('landline/delete/<int:pk>/', LandlineDeleteView.as_view()),
    path('rest/', include(router.urls)),
]
