from django.apps import AppConfig


class LandlineConfig(AppConfig):
    name = 'landline'
