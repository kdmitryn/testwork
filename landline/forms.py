from django.forms import ModelForm
from .models import Landline


# Create the form class
class LandlineForm(ModelForm):
    class Meta:
        model = Landline
        fields = '__all__'
        labels = {
            'll_number': 'Телефон'
            }
