from django.db import models
# from subscriber.models import Subscriber


# Create your models here.
class Landline(models.Model):
    # id / pk = auto increment
    ll_number = models.CharField(
        max_length=19,
        blank=False,
        null=False,
        default='+xxx (xx) xxx xx xx'
    )

    def get_absolute_url(self):
        return f"/landline/list/"

    def __str__(self):
        return self.ll_number
