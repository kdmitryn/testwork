from django.shortcuts import render

from django.urls import path
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Landline
from .forms import LandlineForm

from rest_framework import viewsets
from . serializers import LandlineSerializer


# Create your views here.


# Landline CRUD
class LandlineListView(ListView):
    model = Landline
    template_name = "ll_list.html"


class LandlineCreateView(CreateView):
    model = Landline
    form_class = LandlineForm
    template_name = "ll_create.html"


class LandlineDetailView(DetailView):
    model = Landline
    template_name = "ll_detail.html"


class LandlineUpdateView(UpdateView):
    model = Landline
    form_class = LandlineForm
    template_name = "ll_create.html"


class LandlineDeleteView(DeleteView):
    model = Landline
    success_url = '/landline/list'
    template_name = "ll_delete.html"


class LandlineAPIview(viewsets.ModelViewSet):
    queryset = Landline.objects.all()
    serializer_class = LandlineSerializer
