from django.db import models
from subscriber.models import Subscriber


# Create your models here.
class Mobile(models.Model):
    # id / pk = auto increment
    m_number = models.CharField(
        max_length=19,
        blank=False,
        null=False,
        default='+xxx (xx) xxx xx xx'
    )

    subs = models.ForeignKey(
        Subscriber,
        on_delete=models.CASCADE,
        related_name='mobile',
        blank=True,
        null=True,
    )

    def get_absolute_url(self):
        return f"/mobile/list/"

    def __str__(self):
        return self.m_number
