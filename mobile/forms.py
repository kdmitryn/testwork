from django.forms import ModelForm
from .models import Mobile


# Create the form class
class MobileForm(ModelForm):
    class Meta:
        model = Mobile
        fields = '__all__'
        labels = {
            'm_number': 'Мобильный телефон'
            }
