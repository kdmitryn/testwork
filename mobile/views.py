from django.shortcuts import render

from django.urls import path
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Mobile
from .forms import MobileForm

from rest_framework import viewsets
from . serializers import MobileSerializer

# Create your views here.


# mobile CRUD
class MobileListView(ListView):
    model = Mobile
    template_name = "mobile_list.html"


class MobileCreateView(CreateView):
    model = Mobile
    form_class = MobileForm
    template_name = "mobile_create.html"


class MobileDetailView(DetailView):
    model = Mobile
    template_name = "mobile_detail.html"


class MobileUpdateView(UpdateView):
    model = Mobile
    form_class = MobileForm
    template_name = "mobile_create.html"


class MobileDeleteView(DeleteView):
    model = Mobile
    success_url = '/mobile/list'
    template_name = "mobile_delete.html"


class MobileAPIview(viewsets.ModelViewSet):
    queryset = Mobile.objects.all()
    serializer_class = MobileSerializer
