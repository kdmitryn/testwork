from django.urls import path
from .views import *

from django.urls import include
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'mobile', MobileAPIview)

urlpatterns = [
    path('mobile/list/', MobileListView.as_view()),
    path('mobile/create/', MobileCreateView.as_view()),
    path('mobile/<int:pk>/', MobileDetailView.as_view()),
    path('mobile/update/<int:pk>/', MobileUpdateView.as_view()),
    path('mobile/delete/<int:pk>/', MobileDeleteView.as_view()),
    path('rest/', include(router.urls)),
]
