from rest_framework import serializers
from . models import Subscriber


class SubscriberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subscriber
        fields = [
            'pk',
            'surname',
            'name',
            'patronymic',
            'face_image',
            'e_mail',
            'l_phone',
            ]
