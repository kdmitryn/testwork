from django.urls import path
from .views import *

from django.urls import include
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'subscriber', SubscriberAPIview)

urlpatterns = [
    path('subscriber/list/', SubscriberListView.as_view()),
    path('subscriber/create/', SubscriberCreateView.as_view()),
    path('subscriber/<int:pk>/', SubscriberDetailView.as_view()),
    path('subscriber/update/<int:pk>/', SubscriberUpdateView.as_view()),
    path('subscriber/delete/<int:pk>/', SubscriberDeleteView.as_view()),
    path('PhoneBook/', SubscriberListView.as_view()),
    path('rest/', include(router.urls)),
    ]
