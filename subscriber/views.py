from django.shortcuts import render

from django.urls import path
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Subscriber
from .forms import SubscriberForm

from rest_framework import viewsets
from . serializers import SubscriberSerializer

# Create your views here.


# Subscriber CRUD
class SubscriberListView(ListView):
    model = Subscriber
    template_name = "sub_list.html"


class SubscriberCreateView(CreateView):
    model = Subscriber
    form_class = SubscriberForm
    template_name = "sub_create.html"


class SubscriberDetailView(DetailView):
    model = Subscriber
    template_name = "sub_detail.html"


class SubscriberUpdateView(UpdateView):
    model = Subscriber
    form_class = SubscriberForm
    template_name = "sub_create.html"


class SubscriberDeleteView(DeleteView):
    model = Subscriber
    success_url = '/subscriber/list'
    template_name = "sub_delete.html"


class SubscriberAPIview(viewsets.ModelViewSet):
    queryset = Subscriber.objects.all()
    serializer_class = SubscriberSerializer
