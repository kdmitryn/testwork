from django.db import models
from landline.models import Landline


# Create your models here.


class Subscriber (models.Model):
    surname = models.CharField(
        max_length=30,
        blank=False,
        null=False,
        default=' ')
    name = models.CharField(
        max_length=30,
        blank=False,
        null=False,
        default=' ')
    patronymic = models.CharField(
        max_length=30,
        blank=False,
        null=False,
        default=' ')
    face_image = models.ImageField(
        upload_to='subscriber/static/',
        blank=True,
        null=True)
    e_mail = models.CharField(
        max_length=128,
        blank=False,
        null=False,
        default=' ')

    l_phone = models.ForeignKey(
        Landline,
        on_delete=models.CASCADE,
        related_name='subs',
        null=True,
        blank=True)

    def get_absolute_url(self):
        return f"/subscriber/list/"

    def __str__(self):
        return self.surname + '  ' + self.name + '  ' + self.patronymic
