from django.forms import ModelForm
from .models import Subscriber


# Create the form class
class SubscriberForm(ModelForm):
    class Meta:
        model = Subscriber
        fields = '__all__'
        labels = {
            'face_image': 'Фото',
            'surname': 'Фамилия',
            'name': 'Имя',
            'patronymic': 'Отчество',
            'e_mail': 'e_mail'
            }
