from django.contrib import admin

# Register your models here.
from .models import Subscriber


class SubscriberAdmin(admin.ModelAdmin):
    list_display = [
        'pk',
        'surname',
        'patronymic',
        'face_image',
        'e_mail',
    ]


admin.site.register(Subscriber, SubscriberAdmin)
